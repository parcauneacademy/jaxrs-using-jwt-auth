# README

## Protected URIs

	/rest/*

## Testing the Web Services

### GET Index.html

	curl http://localhost:8080/jaxrs-using-jwt-auth/

### GET ALL Patients (no credentials)
 
	curl -i -H "Accept: application/json" -H "Content-Type: application/json" -v -X GET http://localhost:8080/jaxrs-using-jwt-auth/rest/patients
	

### Authenticate and retrieve Server-Generated JWT TOKEN

	curl -i -H "Accept: application/json" -H "Content-Type: application/json" -v -X POST --data '{"username":"admin","password":"willie"}' http://localhost:8080/jaxrs-using-jwt-auth/rest/auth/login

**Complete output**

	*   Trying ::1...
	* connect to ::1 port 8080 failed: Connection refused
	*   Trying 127.0.0.1...
	* Connected to localhost (127.0.0.1) port 8080 (#0)
	> POST /jaxrs-using-jwt-auth/rest/auth/login HTTP/1.1
	> Host: localhost:8080
	> User-Agent: curl/7.43.0
	> Accept: application/json
	> Content-Type: application/json
	> Content-Length: 40
	> 
	* upload completely sent off: 40 out of 40 bytes
	< HTTP/1.1 200 OK
	HTTP/1.1 200 OK
	< Connection: keep-alive
	Connection: keep-alive
	< X-Powered-By: Undertow/1
	X-Powered-By: Undertow/1
	< Set-Cookie: JSESSIONID=ns_Ypyw4-FjINs7kCnLRR8quN81xUPbxVH76G21B.chieukam; path=/jaxrs-using-jwt-auth
	Set-Cookie: JSESSIONID=ns_Ypyw4-FjINs7kCnLRR8quN81xUPbxVH76G21B.chieukam; path=/jaxrs-using-jwt-auth
	< jwt: eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.TTGx5BC0KbclTFfFDY0BiWJGWfCHOr_tD_2v457wuUTHPCt2WvKb7abDUxzYj6gDzdKPkdaKJwiULaH8E66iVcmw640hOegvuaWLZfFAL0ZLHvK8edVa0sjeH6nnwjj0gEn_jZqV5lXO4VUVKY5JGtGZXPFFtK8Dqi8FLNPJk5lL5m2qcRYbdRFeRJkhO3CkGDEdp5Njy47OhwYzmZ6QaLMgp4fUd6p2S-igMrDj5XKWNfmuvpsoyuQekPFG_lULq4jfeLbmkkpLnVbnmuLWz23_dHE18P2YjehlOZOaJV4FYEBRsCrNj3j5YryTYikip2A7rWxpaXIg_GFQQOdk7Q
	jwt: eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.TTGx5BC0KbclTFfFDY0BiWJGWfCHOr_tD_2v457wuUTHPCt2WvKb7abDUxzYj6gDzdKPkdaKJwiULaH8E66iVcmw640hOegvuaWLZfFAL0ZLHvK8edVa0sjeH6nnwjj0gEn_jZqV5lXO4VUVKY5JGtGZXPFFtK8Dqi8FLNPJk5lL5m2qcRYbdRFeRJkhO3CkGDEdp5Njy47OhwYzmZ6QaLMgp4fUd6p2S-igMrDj5XKWNfmuvpsoyuQekPFG_lULq4jfeLbmkkpLnVbnmuLWz23_dHE18P2YjehlOZOaJV4FYEBRsCrNj3j5YryTYikip2A7rWxpaXIg_GFQQOdk7Q
	< Server: WildFly/10
	Server: WildFly/10
	< Content-Length: 0
	Content-Length: 0
	< Date: Tue, 05 Apr 2016 21:24:43 GMT
	Date: Tue, 05 Apr 2016 21:24:43 GMT
	
	< 
	* Connection #0 to host localhost left intact

	
### GET ALL Patients with Token 

	curl -i -H "Authorization: Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.TTGx5BC0KbclTFfFDY0BiWJGWfCHOr_tD_2v457wuUTHPCt2WvKb7abDUxzYj6gDzdKPkdaKJwiULaH8E66iVcmw640hOegvuaWLZfFAL0ZLHvK8edVa0sjeH6nnwjj0gEn_jZqV5lXO4VUVKY5JGtGZXPFFtK8Dqi8FLNPJk5lL5m2qcRYbdRFeRJkhO3CkGDEdp5Njy47OhwYzmZ6QaLMgp4fUd6p2S-igMrDj5XKWNfmuvpsoyuQekPFG_lULq4jfeLbmkkpLnVbnmuLWz23_dHE18P2YjehlOZOaJV4FYEBRsCrNj3j5YryTYikip2A7rWxpaXIg_GFQQOdk7Q" -H "Accept: application/json" -H "Content-Type: application/json" -v -X GET http://localhost:8080/jaxrs-using-jwt-auth/rest/patients


### GET Patient with ID 1 with Token
	
	curl -i -H "Authorization: Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.TTGx5BC0KbclTFfFDY0BiWJGWfCHOr_tD_2v457wuUTHPCt2WvKb7abDUxzYj6gDzdKPkdaKJwiULaH8E66iVcmw640hOegvuaWLZfFAL0ZLHvK8edVa0sjeH6nnwjj0gEn_jZqV5lXO4VUVKY5JGtGZXPFFtK8Dqi8FLNPJk5lL5m2qcRYbdRFeRJkhO3CkGDEdp5Njy47OhwYzmZ6QaLMgp4fUd6p2S-igMrDj5XKWNfmuvpsoyuQekPFG_lULq4jfeLbmkkpLnVbnmuLWz23_dHE18P2YjehlOZOaJV4FYEBRsCrNj3j5YryTYikip2A7rWxpaXIg_GFQQOdk7Q" -H "Accept: application/json" -H "Content-Type: application/json" -v -X GET http://localhost:8080/jaxrs-using-jwt-auth/rest/patients/1
	