package org.parcaune.security.dto;

import java.io.Serializable;

public class ErrorDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	private String context;

	private String message;

	public ErrorDetail() {
		super();
	}

	public ErrorDetail(String message) {
		this(null, message);
	}

	public ErrorDetail(String context, String message) {
		this.context = context;
		this.message = message;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Error [context=" + context + ", message=" + message + "]";
	}

}
