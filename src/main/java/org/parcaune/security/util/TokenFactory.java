package org.parcaune.security.util;

import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.lang.JoseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TokenFactory {

	private static final Logger LOG = LoggerFactory.getLogger(TokenFactory.class);

	public static String buildJsonWebToken(String subject) {
		RsaJsonWebKey rsaJsonWebKey = RsaKeyProducer.getInstance().getKey();
		
        JwtClaims claims = new JwtClaims();
        claims.setSubject(subject); // the subject/principal is whom the token is about
		
        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setKey(rsaJsonWebKey.getPrivateKey());
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
		
        String jwt = null;
        
        try {
        	jwt = jws.getCompactSerialization();
		} catch (JoseException e) {
            LOG.error(e.getMessage(), e);
		}
        
        LOG.info("Claim: {}, JWS: {}, JWT: {} ", claims, jws, jwt);

		return jwt;
	}

}
