package org.parcaune.security.util;

import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.keys.RsaKeyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Singleton
 * 
 * @author chieukam
 */
public class RsaKeyProducer {

	private static final Logger LOG = LoggerFactory.getLogger(RsaKeyProducer.class);

	private static RsaKeyProducer instance = new RsaKeyProducer();

	private RsaJsonWebKey jsonWebKey;

	/*
	 * A private Constructor prevents any other class from instantiating.
	 */
	private RsaKeyProducer() {
		produce();
	}

	/* Static 'instance' method */
	public static RsaKeyProducer getInstance() {
		return instance;
	}

	private RsaJsonWebKey produce() {
		if (jsonWebKey == null) {
			try {
				jsonWebKey = RsaJwkGenerator.generateJwk(2048);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
		String pemEncode = RsaKeyUtil.pemEncode(jsonWebKey.getRsaPublicKey());
		LOG.debug("RSA Public Key PEM Encode: {}\n", pemEncode);
		
		LOG.debug("RSA Key setup: {}", jsonWebKey.hashCode());
		return jsonWebKey;
	}

	public RsaJsonWebKey getKey() {
		return jsonWebKey;
	}

}
