package org.parcaune.security.filter;

import java.io.IOException;
import java.util.Arrays;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import org.parcaune.security.annotation.Secured;
import org.parcaune.security.util.TokenFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Secured
@Provider
public class TokenResponseFilter implements ContainerResponseFilter {

	private static final Logger LOG = LoggerFactory.getLogger(TokenResponseFilter.class);

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		LOG.debug("Entering the response filter ...");

		if (requestContext.getProperty("auth-failed") != null) {

			Boolean failed = (Boolean) requestContext.getProperty("auth-failed");
			if (failed) {
				LOG.info("JWT auth failed. No need to return JWT token");
				return;
			}
		}

		responseContext.getHeaders().put("jwt", Arrays
				.asList(TokenFactory.buildJsonWebToken(requestContext.getSecurityContext().getUserPrincipal().getName())));
		LOG.info("Added JWT to response header 'jwt'");
	}

}
