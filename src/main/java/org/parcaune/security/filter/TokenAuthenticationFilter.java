package org.parcaune.security.filter;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.parcaune.security.annotation.Secured;
import org.parcaune.security.dto.ErrorDetail;
import org.parcaune.security.util.RsaKeyProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class TokenAuthenticationFilter implements ContainerRequestFilter {

	private static final Logger LOG = LoggerFactory.getLogger(TokenAuthenticationFilter.class);

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		LOG.info("Entering the request filter ...");

		String authorization = requestContext.getHeaderString("Authorization");

		// Execute signature validation
		if (authorization != null && authorization.startsWith("Bearer")) {
			try {
				LOG.info("JWT based Auth: signature verification...");
				String jwtoken = authorization.split(" ")[1];
				validate(jwtoken);
			} catch (InvalidJwtException e) {
				LOG.error("JWT validation failed", e);

				requestContext.setProperty("auth-failed", true);
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
						.entity(new ErrorDetail(String.format("JWT validation failed: %s", e.getMessage()))).build());
			}
		} else {
			LOG.error("No JWT token !");
			requestContext.setProperty("auth-failed", true);
			requestContext.abortWith(
					Response.status(Response.Status.UNAUTHORIZED).entity(new ErrorDetail("No JWT token!")).build());
		}
	}

	private String validate(String jwt) throws InvalidJwtException {
		// Should be the same as the one used to build the JWT previously
		RsaJsonWebKey rsaJsonWebKey = RsaKeyProducer.getInstance().getKey();

		LOG.debug("RSA hash code... {}", rsaJsonWebKey.hashCode());

		JwtConsumerBuilder jwtConsumerBuilder = new JwtConsumerBuilder();
		// the JWT must have a subject claim
		jwtConsumerBuilder.setRequireSubject();
		// verify the signature with the public key
		jwtConsumerBuilder.setVerificationKey(rsaJsonWebKey.getKey());
		// create the JwtConsumer instance
		JwtConsumer jwtConsumer = jwtConsumerBuilder.build();

		try {
			// Validate the JWT and process it to the Claims
			JwtClaims jwtClaims = jwtConsumer.processToClaims(jwt);
			String subject = (String) jwtClaims.getClaimValue("sub");
			LOG.info("JWT Token validation succeeded! {}", jwtClaims);
			return subject;
		} catch (InvalidJwtException e) {
			LOG.error(String.format("JWT Token validation failed! %s", e.getMessage()), e);
			throw e;
		}
	}

}
