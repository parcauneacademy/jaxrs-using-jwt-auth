package org.parcaune.security.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.parcaune.security.annotation.Secured;
import org.parcaune.security.dto.ErrorDetail;
import org.parcaune.security.dto.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test resource
 */
@Secured
@Path("patients")
public class PatientResource {

	private static final Logger LOG = LoggerFactory.getLogger(PatientResource.class);

	static Map<String, Patient> patients = new HashMap<String, Patient>();

	@Context
	SecurityContext securityContext;

	static {
		LOG.info("Initializing the database...");
		patients.put("1", new Patient("1", "John", "Doe"));
		patients.put("2", new Patient("2", "Lo", "Konboi"));
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Patient> getAllPatients() {
		LOG.info("Getting all Patients...");

		ArrayList<Patient> list = new ArrayList<Patient>(patients.values());
		return list;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPatientById(@PathParam("id") String id) {
		LOG.info("Getting Patient by ID: " + id);

		Patient patient = patients.get(id);
		if (patient != null) {
			return Response.ok(patient).build();
		}
		return Response.status(Status.NOT_FOUND)
				.entity(new ErrorDetail(String.format("Patient with id %s not found", id))).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createPatient(Patient patient) {
		LOG.info("Adding patient: " + patient);

		if (patient.getId() != null && !patient.getId().isEmpty()) {
			return Response.status(Status.NOT_ACCEPTABLE)
					.entity(new ErrorDetail(String.format("Patient with id %s already exists", patient.getId())))
					.build();
		}
		patient.setId(UUID.randomUUID().toString());
		patients.put(patient.getId(), patient);
		return Response.ok(patient).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePatient(@PathParam("id") String id, Patient patient) {
		LOG.info("Updating patient with ID: {} ", id);
		
		Patient old = patients.get(id);
		if (old != null) {
			patients.put(id, patient);
			return Response.ok(patient).build();
		}
		return Response.status(Status.NOT_FOUND)
				.entity(new ErrorDetail(String.format("Patient with ID: %s does NOT exist...", id))).build();
	}

	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deletePatientById(@PathParam("id") String id) {
		LOG.info("Deleting patient with ID: {}", id);

		Patient patient = patients.remove(id);
		if (patient != null) {
			return Response.ok(patient).build();
		}
		return Response.status(Status.NOT_FOUND)
				.entity(new ErrorDetail(String.format("Patient with ID: %s does NOT exist...", id))).build();
	}
}
