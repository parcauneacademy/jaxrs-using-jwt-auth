package org.parcaune.security.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.parcaune.security.annotation.Secured;

/**
 * UserResource
 * 
 * @author chieukam
 */
@Secured
@Path("users")
public class UserResource {

	@Context
	SecurityContext securityContext;

	@GET
	public Response sayWelcome() {
		Response response = Response
				.ok("Welcome to the JWT Social Network! " + securityContext.getUserPrincipal().getName()).build();

		return response;
	}

}