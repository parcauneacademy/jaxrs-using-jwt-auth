package org.parcaune.security.resource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.parcaune.security.dto.Credentials;
import org.parcaune.security.dto.ErrorDetail;
import org.parcaune.security.util.TokenFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * AuthResource
 * 
 * @author chieukam
 */
@Path("/auth")
public class AuthResource {

	private static final Logger LOG = LoggerFactory.getLogger(AuthResource.class);

	@Context
	HttpServletRequest request;

	@Context
	SecurityContext securityContext;

	/**
	 * Login and return the JWT token in response header
	 * 
	 * @param userInfo
	 * @return
	 */
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(Credentials userInfo) {
		try {
			request.login(userInfo.getUsername(), userInfo.getPassword());
			LOG.debug("Authenticated user: {}", securityContext.getUserPrincipal().getName());

			return Response.ok().header("jwt", TokenFactory.buildJsonWebToken(securityContext.getUserPrincipal().getName()))
					.build();

		} catch (ServletException e) {
			LOG.error("Login Failed.", e);
			return Response.status(Status.UNAUTHORIZED).entity(new ErrorDetail(e.getMessage())).build();
		}
	}

	@GET
	@Path("/logout")
	@Produces(MediaType.APPLICATION_JSON)
	public Response logout() {
		try {
			request.logout();
			LOG.debug("Authenticated user: {}", securityContext.getUserPrincipal());

			return Response.ok().build();
		} catch (ServletException e) {
			LOG.error("Logout Failed.", e);
			return Response.serverError().entity(new ErrorDetail(e.getMessage())).build();
		}
	}

}
